Array.prototype.forEach.call(document.getElementsByClassName("face"), function(element) {
    element.addEventListener("click", function() {
        element.style.backgroundColor = "transparent";
        element.style.pointerEvents = "none";
    }, true);
}, this);